//
//  Configuracion.m
//  MFSideMenuDemoStoryboard
//
//  Created by Carlos Arturo Medina García on 21/02/14.
//  Copyright (c) 2014 Michael Frederick. All rights reserved.
//

#import "Configuracion.h"
#import "MFSideMenu.h"


@implementation Configuracion{
    NSArray * objetos;
}

- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    NSLog(@"Pulsaron: %d",buttonIndex);
    UIAlertView * alerta = [[UIAlertView alloc] initWithTitle:@"Holi" message:[actionSheet buttonTitleAtIndex:buttonIndex] delegate:nil cancelButtonTitle:@"Cerrar" otherButtonTitles:nil, nil];
    [alerta show];

}


- (void)showActionSheet:(id)sender
{
    NSString *actionSheetTitle = @"Action Sheet Demo"; //Action Sheet Title
    NSString *destructiveTitle = @"Destructive Button"; //Action Sheet Button Titles
    NSString *other1 = @"Other Button 1";
    NSString *other2 = @"Other Button 2";
    NSString *other3 = @"Other Button 3";
    NSString *cancelTitle = @"Cancel Button";
    
    UIActionSheet *actionSheet = [[UIActionSheet alloc]
                                  initWithTitle:actionSheetTitle
                                  delegate:self
                                  cancelButtonTitle:cancelTitle
                                  destructiveButtonTitle:destructiveTitle
                                  otherButtonTitles:other1, other2, other3, nil];
    
    [actionSheet showInView:[UIApplication sharedApplication].keyWindow];
    
    //Get the name of the current pressed button
    
    
}



-(void)viewDidLoad{
    objetos = [NSArray arrayWithObjects:@"Mexico",@"Alemania",@"Brasil", nil];
   /* UIButton *button = [UIButton buttonWithType:UIButtonTypeRoundedRect];
    button.frame = CGRectMake(20.0f, 186.0f, 280.0f, 88.0f);
    [button setTitle:@"Show Action Sheet" forState:UIControlStateNormal];
    [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
    button.tintColor = [UIColor darkGrayColor];
    [button addTarget:self action:@selector(showActionSheet:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:button];*/
}

- (IBAction)LanzaOpciones:(id)sender {
    [self showActionSheet:self];
}

- (IBAction)abrirmenu:(id)sender {
        [self.menuContainerViewController toggleLeftSideMenuCompletion:nil];
}

#pragma mark UITableViewDataSource, UITableViewDelegate

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return objetos.count;
}

-(int)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    static NSString * identificador = @"CeldaConfiguracion";
    UITableViewCell * celda = (UITableViewCell *)[tableView dequeueReusableCellWithIdentifier:identificador];
    celda.textLabel.text = [objetos objectAtIndex:indexPath.row];
    return celda;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    UIAlertView * alerta = [[UIAlertView alloc] initWithTitle:@"Holi" message:[objetos objectAtIndex:indexPath.row] delegate:nil cancelButtonTitle:@"Cerrar" otherButtonTitles:nil, nil];
    [alerta show];
}
@end
