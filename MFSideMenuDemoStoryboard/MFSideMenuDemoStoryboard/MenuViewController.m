//
//  MenuViewController.m
//  MFSideMenuDemoStoryboard
//
//  Created by Daniel García Alvarado on 18/02/14.
//  Copyright (c) 2014 Michael Frederick. All rights reserved.
//

#import "MenuViewController.h"
#import "MFSideMenu.h"
#import "PerfilViewController.h"
#import "DemoViewController.h"
#import "Configuracion.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    self.tableView.bounds = CGRectMake(0, 20, self.view.bounds.size.height, self.view.bounds.size.width);
    self.tableView.layer.masksToBounds = YES;
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view delegate

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"Pulsaron: %d",indexPath.row);
    
    switch (indexPath.row) {
        case 0:{
                PerfilViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"perfil"];
                UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
                NSArray *controllers = [NSArray arrayWithObject:controller];
                navigationController.viewControllers = controllers;
                break;
            }
        case 1:{
            DemoViewController *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"DemoViewController"];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:controller];
            navigationController.viewControllers = controllers;
            break;
            
        }
        case 2:{
             Configuracion *controller = [self.storyboard instantiateViewControllerWithIdentifier:@"Configuracion"];
            UINavigationController *navigationController = self.menuContainerViewController.centerViewController;
            NSArray *controllers = [NSArray arrayWithObject:controller];
            navigationController.viewControllers = controllers;
            break;
        }
        default:
            break;
    }
    
    [self.menuContainerViewController setMenuState:MFSideMenuStateClosed];
}

@end
