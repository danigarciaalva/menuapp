//
//  MenuViewController.h
//  MFSideMenuDemoStoryboard
//
//  Created by Daniel García Alvarado on 18/02/14.
//  Copyright (c) 2014 Michael Frederick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MenuViewController : UITableViewController

@end
