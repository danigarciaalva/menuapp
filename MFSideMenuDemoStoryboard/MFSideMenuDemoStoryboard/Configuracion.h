//
//  Configuracion.h
//  MFSideMenuDemoStoryboard
//
//  Created by Carlos Arturo Medina García on 21/02/14.
//  Copyright (c) 2014 Michael Frederick. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface Configuracion : UIViewController <UITableViewDataSource, UITableViewDelegate, UIActionSheetDelegate>
- (IBAction)LanzaOpciones:(id)sender;
- (IBAction)abrirmenu:(id)sender;
@end
